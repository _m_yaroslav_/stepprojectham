const tabs = document.querySelectorAll('.services-tabs-title');
const tabContents = document.querySelectorAll('.tabs-content li');

tabs.forEach((tab, index) => {
    tab.addEventListener('click', () => {
        tabContents.forEach(content => {
            content.style.display = 'none';
        });
        tabContents[index].style.display = 'flex';

        tabs.forEach(tab => {
            tab.classList.remove('active');
        });
        tab.classList.add('active');
    });
});
