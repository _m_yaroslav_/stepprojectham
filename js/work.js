// Load more button functionality
const loadMoreButton = document.querySelector('.button-load-work');
const workBlocks = document.querySelector('.work-blocks');


loadMoreButton.addEventListener('click', function(click) {
    click.preventDefault()
    const newImages = `
    
                <div class="work-blocks">
                    <div class="work-blocks-row">
                        <div class="work-blocks-item graphic" data-attribute="graphic" >
                            <img src="./img/graphic%20design/graphic-design1.jpg" alt="graphic">
                            <div class="work-blocks-item-details">
                                <div class="item-details-links">
                                    <a href="#"></a>
                                    <a href="#" class="item-link icon-chain"></a>
                                    <a href="#" class="item-link"><span></span></a>
                                </div>
                                <div class="item-details-title">creative design</div>
                                <div class="item-details-subtitle">Graphic Design</div>
                            </div>
                        </div>

                        <div class="work-blocks-item graphic" data-attribute="graphic">
                            <img src="./img/graphic%20design/graphic-design2.jpg" alt="web">
                            <div class="work-blocks-item-details">
                                <div class="item-details-links">
                                    <a href="#"></a>
                                    <a href="#" class="item-link icon-chain"></a>
                                    <a href="#" class="item-link"><span></span></a>
                                </div>
                                <div class="item-details-title">creative design</div>
                                <div class="item-details-subtitle">Graphic Design</div>
                            </div>
                        </div>

                        <div class="work-blocks-item graphic" data-attribute="graphic">
                            <img src="./img/graphic%20design/graphic-design3.jpg" alt="landing">
                            <div class="work-blocks-item-details">
                                <div class="item-details-links">
                                    <a href="#"></a>
                                    <a href="#" class="item-link icon-chain"></a>
                                    <a href="#" class="item-link"><span></span></a>
                                </div>
                                <div class="item-details-title">creative design</div>
                                <div class="item-details-subtitle">Graphic Design</div>
                            </div>
                        </div>

                        <div class="work-blocks-item graphic" data-attribute="web">
                            <img src="./img/graphic%20design/graphic-design4.jpg" alt="wordpress">
                            <div class="work-blocks-item-details">
                                <div class="item-details-links">
                                    <a href="#"></a>
                                    <a href="#" class="item-link icon-chain"></a>
                                    <a href="#" class="item-link"><span></span></a>
                                </div>
                                <div class="item-details-title">creative design</div>
                                <div class="item-details-subtitle">Graphic Design</div>
                            </div>
                        </div>

                        <div class="work-blocks-item graphic" data-attribute="web">
                            <img src="./img/graphic%20design/graphic-design5.jpg" alt="graphic">
                            <div class="work-blocks-item-details">
                                <div class="item-details-links">
                                    <a href="#"></a>
                                    <a href="#" class="item-link icon-chain"></a>
                                    <a href="#" class="item-link"><span></span></a>
                                </div>
                                <div class="item-details-title">creative design</div>
                                <div class="item-details-subtitle">Graphic Design</div>
                            </div>
                        </div>

                        <div class="work-blocks-item graphic" data-attribute="web">
                            <img src="./img/graphic%20design/graphic-design6.jpg" alt="graphic">
                            <div class="work-blocks-item-details">
                                <div class="item-details-links">
                                    <a href="#"></a>
                                    <a href="#" class="item-link icon-chain"></a>
                                    <a href="#" class="item-link"><span></span></a>
                                </div>
                                <div class="item-details-title">creative design</div>
                                <div class="item-details-subtitle">Graphic Design</div>
                            </div>
                        </div>

                        <div class="work-blocks-item graphic" data-attribute="landing">
                            <img src="./img/graphic%20design/graphic-design7.jpg" alt="graphic">
                            <div class="work-blocks-item-details">
                                <div class="item-details-links">
                                    <a href="#"></a>
                                    <a href="#" class="item-link icon-chain"></a>
                                    <a href="#" class="item-link"><span></span></a>
                                </div>
                                <div class="item-details-title">creative design</div>
                                <div class="item-details-subtitle">Graphic Design</div>
                            </div>
                        </div>

                        <div class="work-blocks-item graphic" data-attribute="landing">
                            <img src="./img/graphic%20design/graphic-design8.jpg" alt="graphic">
                            <div class="work-blocks-item-details">
                                <div class="item-details-links">
                                    <a href="#"></a>
                                    <a href="#" class="item-link icon-chain"></a>
                                    <a href="#" class="item-link"><span></span></a>
                                </div>
                                <div class="item-details-title">creative design</div>
                                <div class="item-details-subtitle">Graphic Design</div>
                            </div>
                        </div>

                        <div class="work-blocks-item graphic" data-attribute="landing">
                            <img src="./img/graphic%20design/graphic-design9.jpg" alt="graphic">
                            <div class="work-blocks-item-details">
                                <div class="item-details-links">
                                    <a href="#"></a>
                                    <a href="#" class="item-link icon-chain"></a>
                                    <a href="#" class="item-link"><span></span></a>
                                </div>
                                <div class="item-details-title">creative design</div>
                                <div class="item-details-subtitle">Graphic Design</div>
                            </div>
                        </div>

                        <div class="work-blocks-item graphic" data-attribute="wordpress">
                            <img src="./img/graphic%20design/graphic-design10.jpg" alt="graphic">
                            <div class="work-blocks-item-details">
                                <div class="item-details-links">
                                    <a href="#"></a>
                                    <a href="#" class="item-link icon-chain"></a>
                                    <a href="#" class="item-link"><span></span></a>
                                </div>
                                <div class="item-details-title">creative design</div>
                                <div class="item-details-subtitle">Graphic Design</div>
                            </div>
                        </div>

                        <div class="work-blocks-item graphic" data-attribute="wordpress">
                            <img src="./img/graphic%20design/graphic-design11.jpg" alt="graphic">
                            <div class="work-blocks-item-details">
                                <div class="item-details-links">
                                    <a href="#"></a>
                                    <a href="#" class="item-link icon-chain"></a>
                                    <a href="#" class="item-link"><span></span></a>
                                </div>
                                <div class="item-details-title">creative design</div>
                                <div class="item-details-subtitle">Graphic Design</div>
                            </div>
                        </div>

                        <div class="work-blocks-item graphic" data-attribute="wordpress">
                            <img src="./img/graphic%20design/graphic-design12.jpg" alt="graphic">
                            <div class="work-blocks-item-details">
                                <div class="item-details-links">
                                    <a href="#"></a>
                                    <a href="#" class="item-link icon-chain"></a>
                                    <a href="#" class="item-link"><span></span></a>
                                </div>
                                <div class="item-details-title">creative design</div>
                                <div class="item-details-subtitle">Graphic Design</div>
                            </div>
                        </div>
  `;

    workBlocks.insertAdjacentHTML('beforeend', newImages);
    loadMoreButton.style.display = 'none';
});

// Filter functionality
const workListItems = document.querySelectorAll('.work-list-item');
const workBlocksItems = document.querySelectorAll('.work-blocks-item');

workListItems.forEach(function(item) {
    item.addEventListener('click', function() {
        const categoryId = this.getAttribute('data-id');
        console.log(categoryId)

        if (categoryId === 'all') {
            workBlocksItems.forEach(function(block) {
                block.style.display = 'block';
            });
        } else {
            workBlocksItems.forEach(function(block) {
                const blockCategoryId = block.getAttribute('data-attribute');
                console.log(blockCategoryId)

                if (blockCategoryId === categoryId) {

                    block.style.display = 'block';
                } else {
                    block.style.display = 'none';
                }
            });
        }
    });
});


