// Отримання всіх необхідних елементів
const sliderFor = document.querySelector('.slider-for');
const items = sliderFor.querySelectorAll('.item');
const sliderNav = document.querySelector('.slider-naw');
const prevButton = sliderNav.querySelector('.buttons-slider:first-child');
const nextButton = sliderNav.querySelector('.buttons-slider:last-child');
const sliderLi = sliderNav.querySelectorAll('.slider-li li');

// Перевірка, чи існують елементи каруселі
if (items.length > 0) {
    // Встановлення початкового стану
    let currentIndex = 0;
    showItem(currentIndex);

    // Обробник кліку на кнопку "Попередній слайд"
    prevButton.addEventListener('click', () => {
        currentIndex = (currentIndex - 1 + items.length) % items.length;
        showItem(currentIndex);
        updateIconAnimation('slide-prev');
    });

    // Обробник кліку на кнопку "Наступний слайд"
    nextButton.addEventListener('click', () => {
        currentIndex = (currentIndex + 1) % items.length;
        showItem(currentIndex);
        updateIconAnimation('slide-next');
    });

    // Обробник кліку на елементи навігації
    sliderLi.forEach((li, index) => {
        li.addEventListener('click', () => {
            currentIndex = index;
            showItem(currentIndex);
            updateIconAnimation('');
        });
    });

    // Функція показу поточного елемента
    function showItem(index) {
        // Сховати всі елементи
        items.forEach(item => {
            item.classList.remove('active');
        });

        // Показати поточний елемент
        items[index].classList.add('active');
    }

    // Функція оновлення анімації для іконок
    function updateIconAnimation(animationClass) {
        sliderLi.forEach((li, index) => {
            if (index === currentIndex) {
                li.classList.add(animationClass);
            } else {
                li.classList.remove('slide-prev', 'slide-next');
            }
        });
    }
}
